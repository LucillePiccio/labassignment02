package application;
//importing vehicles package
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        //creating vehicles object
        Bicycle bike = new vehicles.Bicycle("", 0, 0);
        
        //creating bike array and initizaling
        Bicycle[] bikeArr = new Bicycle[4];
        bikeArr[0] = new Bicycle("Giant",8,20);
        bikeArr[1] = new Bicycle("Bianchi", 10, 30);
        bikeArr[2] = new Bicycle("Trek", 9, 30);
        bikeArr[3] = new Bicycle("Specialized", 11, 20);
    
        //testing Bicycle array
       for(int i = 0; i < bikeArr.length; i++){
        System.out.println(bikeArr[i]);
       }
    }
}
