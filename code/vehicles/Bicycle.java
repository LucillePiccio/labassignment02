package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //get methods
    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpedd(){
        return this.maxSpeed;
    }

    //toString method
    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " +
        this.maxSpeed;
    }
}
